package com.company.manager;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * @author Daniel Palonek (palonek.daniel@gmail.com)
 */
@SpringBootApplication
public class SessionManagerApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(SessionManagerApplication.class)
                .build()
                .run(args);
    }

}
