package com.company.manager.controller;

import com.company.manager.service.SessionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

/**
 * @author Daniel Palonek (palonek.daniel@gmail.com)
 */
@RestController
@RequestMapping("session")
@Log4j2
@RequiredArgsConstructor
class SessionController {

    private static final String X_AUTH_TOKEN = "X-Auth-Token";

    private final SessionService sessionService;

    @PostMapping
    @Transactional
    ResponseEntity<String> createSession() {
        String sessionId = sessionService.reuseSession();
        if (sessionId != null) {
            return ResponseEntity.ok(sessionId);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping
    @Transactional(readOnly = true)
    ResponseEntity<Void> checkSession(@RequestHeader(X_AUTH_TOKEN) String sessionId) {
        Boolean isActive = sessionService.checkSession(sessionId);
        if (isActive != null && isActive) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping
    @Transactional
    ResponseEntity<Void> keepSessionAlive(@RequestHeader(X_AUTH_TOKEN) String sessionId) {
        Boolean keepAlive = sessionService.keepSessionAlive(sessionId);
        if (keepAlive != null && keepAlive) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping
    @Transactional
    ResponseEntity<Void> closeSession(@RequestHeader(X_AUTH_TOKEN) String sessionId) {
        Boolean wasClosed = sessionService.releaseSession(sessionId);
        if (wasClosed != null && wasClosed) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
