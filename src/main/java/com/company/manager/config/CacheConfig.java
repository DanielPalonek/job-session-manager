package com.company.manager.config;

import io.micrometer.core.instrument.util.StringUtils;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Daniel Palonek (palonek.daniel@gmail.com)
 */
@Configuration
public class CacheConfig {

    private final String redisHost;
    private final int redisPort;
    private final String redisPass;

    @Autowired
    public CacheConfig(
            @Value("${spring.redis.host}") final String redisHost,
            @Value("${spring.redis.port}") int redisPort,
            @Value("${spring.redis.password}") final String redisPass
    ) {
        this.redisHost = redisHost;
        this.redisPort = redisPort;
        this.redisPass = redisPass;
    }

    @Bean(destroyMethod="shutdown")
    public RedissonClient redissonClient() {
        final Config config = new Config();
        if(StringUtils.isNotEmpty(this.redisPass)) {
            config.useSingleServer()
                    .setAddress("redis://" + this.redisHost + ":" + this.redisPort)
                    .setPassword(this.redisPass);
        } else {
            config.useSingleServer()
                    .setAddress("redis://" + this.redisHost + ":" + this.redisPort);
        }
        return Redisson.create(config);
    }

}
