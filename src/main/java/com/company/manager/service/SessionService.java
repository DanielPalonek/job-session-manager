package com.company.manager.service;

import org.redisson.api.LocalCachedMapOptions;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Daniel Palonek (palonek.daniel@gmail.com)
 */
@Service
public class SessionService {

    private final RedissonClient redissonClient;

    private final RMap<String, Boolean> sessionMap;

    private final int sessionMaxSize;

    public SessionService(
            RedissonClient redissonClient,
            @Value("${session.max-size}") final int sessionMaxSize
    ) {
        this.redissonClient = redissonClient;
        this.sessionMaxSize = sessionMaxSize;
        // should be set to max TTL in case of worker die and abandoned session
        this.sessionMap = this.redissonClient.getLocalCachedMap("session" , LocalCachedMapOptions.defaults());
        this.sessionMap.putAll(createSessionPool());
    }

    public String reuseSession() {
        // use lock mechanism, ie RLock - not enough time to implement it
        Optional<String> sessionId = sessionMap.entrySet().stream()
                .filter(e -> !e.getValue())
                .findFirst()
                .map(Map.Entry::getKey);
        String resultSessionId = null;
        if (sessionId.isPresent()) {
            resultSessionId = sessionId.get();
            sessionMap.put(sessionId.get(), true);
        }
        return resultSessionId;
    }

    public Boolean checkSession(String sessionId) {
        return sessionMap.get(sessionId);
    }

    public Boolean keepSessionAlive(String sessionId) {
        return sessionMap.putIfExists(sessionId, true);
    }

    public Boolean releaseSession(String sessionId) {
        return sessionMap.putIfExists(sessionId, false);
    }

    private Map<String, Boolean> createSessionPool() {
        Map<String, Boolean> sessionPool = new HashMap<>();
        // can be used some mechanism, which replace multiple used sessions on new ones after releasing to ensure new session circulating
        int itemsToLoad = sessionMaxSize - sessionMap.size();
        for(int i = 0; i < itemsToLoad; i++) {
            sessionPool.put(UUID.randomUUID().toString(), false);
        }
        return sessionPool;
    }


}
