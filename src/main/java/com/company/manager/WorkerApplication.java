package com.company.manager;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

/**
 * @author Daniel Palonek (palonek.daniel@gmail.com)
 */
@SpringBootApplication
@Log4j2
public class WorkerApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(WorkerApplication.class).properties("server.port=8081")
                .build()
                .run(args);
    }

    // should be commented when starting SessionManagerApplication
    @PostConstruct
    public void makeWorkerAction() throws InterruptedException {
        RestTemplate restTemplate = new RestTemplate();
        log.info("Calling for new session.");
        ResponseEntity<String> sessionResponse = restTemplate.postForEntity("http://localhost:8080/session", null, String.class);
        if (sessionResponse.getStatusCode() == HttpStatus.OK) {
            log.info("Starting to work with sessionId = {}.", sessionResponse.getBody());
            TimeUnit.SECONDS.sleep(10);
            log.info("Releasing session.");
            MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
            headers.set("X-Auth-Token", sessionResponse.getBody());
            HttpEntity<?> entity = new HttpEntity<>(headers);
            restTemplate.exchange("http://localhost:8080/session", HttpMethod.DELETE, entity, Void.class);
        }

    }

}
